<html>
    <head>
    </head>
    <body>
        <table align="center" width="100%" height="100%" style="background-color:#f2f2f2">
            <tbody>
                <tr>
                    <td align="center" valign="top" style="padding:20px">
                        <table width="600">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                  <div style="background-color:#F39C12;color:#FFFFFF;height:10px;width:100%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                   <a href="{{ Config('app.url') }}" target="_blank" style="color:#FFFFFF;text-decoration: none;"><h1 style="padding-left:28px; padding-top:10px;"></h1></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="background-color:#FFFFFF">
                                                        <table style="width:100%">
                                                            <tr>
                                                                <td style="padding:30px 30px 20px 30px">
                                                                    <p style="font-family:Roboto-Regular,Open Sans,Helvetica, Arial, sans-serif">Hello<strong> {{ $full_name }}</strong>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p style="padding-left:30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif">Thank you for visiting our office
                                                                    </p> 
                                                                    <?php if($tracking_id !="") {?>
                                                                    <p style="padding-left:30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif"> Your ticket has been generated <strong>{{ $tracking_id }}</strong>
                                                                    </p> <br>
                                                                    <p style="padding-left:30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif">Kindly use below link to track your ticket <br>
                                                                    <a href="{{ Config('app.url') }}track/schedule" target="_blank">{{ Config('app.url') }}track/schedule</a>
                                                                    </p> 
                                                                    <?php }?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p style="margin:0px 30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif">Best Regards, 
                                                                    <p style="margin:0px 30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif"><b>Prasad Lad</b>
                                                                    </p>
                                                                    <p style="margin:0px 30px;font-family:Roboto-Regular,Open Sans, Helvetica, Arial, sans-serif">MLC & Vice President BJP- Maharastra
                                                                    </p>
                                                                    </p><br/>
                                                                </td>
                                                            </tr>
                                                        </table>                                                   

                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>