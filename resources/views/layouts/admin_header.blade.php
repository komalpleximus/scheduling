<!DOCTYPE html>
<html>
  <head>
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">-->
    <meta content="utf-8" http-equiv="encoding" charset=UTF-8>
    <title>Prasad Lad</title>
    <link rel="shortcut icon" href="#">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('/date-range/daterangepicker.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('/font-awesome/css/font-awesome.min.css')}}" type="text/css">


    {{--<script src="{{ asset('/js/moment.js') }}"></script>--}}
    {{--<script src="{{ asset('/js/daterangepicker.js') }}"></script>--}}
    <!-- Slick -->
    <link href="{{ asset('/css/slick/slick.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/slick/slick-theme.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link href="{{ asset('/plugins/select2/select2.min.css') }}" rel="stylesheet">   
    <!-- DataTables -->
    <link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('/css/theme.min.css') }}" rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('/css/all-skins.min.css') }}" rel="stylesheet">
    <!--link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet"-->
    <link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet">
    <!-- jvectormap -->
    <link href="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/datepicker.css') }}" rel="stylesheet" type="text/css">
    <!-- Bootstrap time Picker -->
    <link href="{{ asset('/plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/editor.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/scheduling_admin.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
    <script src="{{asset('/date-range/moment.js')}}" ></script>
    <link rel="shortcut icon" href="/images/favicon.ico">
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
	<body class="hold-transition skin-red sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>PL</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Prasad Lad</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">   
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">12400</span>
                  <span class="label label-success hide">{{ $sms_balance }}</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header hide">Your SMS Balance : {{ $sms_balance }}</li>
                  <li class="header">Your SMS Balance : 12400</li>
                </ul>
              </li>            
              <!--li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">0</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 0 notifications</li>
                </ul>
              </li-->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs"><i class="fa fa-user  sm-padding-right"></i>&nbsp;&nbsp;&nbsp;<?php echo strtoupper(Auth::user()->name)?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <!--li class="user-header">
                  <p>
                    <?php 
                      $name = Auth::user()->name;
                      $user_type = Auth::user()->user_type;
                      $image = Auth::user()->image;
                    ?>
                    <?php echo strtoupper($name)?>
                    @if($user_type ==0)
                      <small>Admin</small>
                    @else
                      <small>User</small>
                    @endif
                  </p>
                  <?php echo '<img src="/images/unknown.jpg" class="img-circle" alt="User Image"" />'; ?>
                  </li-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <!--div class="pull-left">
                      <a href="/user/profile" class="btn btn-default btn-flat">Profile</a>
                    </div-->
                    <div class="text-center">
                      <a href="{{url('user/logout')}}" class="btn btn-primary btn-flat">Log out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the sidebar -->
     @include('admin.sidebar_nav')
      <div class="content-wrapper">
        <div class="content-div">
          @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable alert-box">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

              {{ Session::get('success') }}				
            </div>
          @endif
          @if(Session::has('error'))
            @if(is_array(session('error')))
              <div class="alert alert-danger alert-dismissable alert-box">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;   </button>
                @foreach(Session::get('error') as $erros)
                  @for($i=0;$i<count($erros);$i++)
                    <ul>
                      <li>{{ $erros[$i] }}</li>
                    </ul>
                  @endfor
                @endforeach
              </div>
            @endif
          @endif
            {{--@if (count($errors) > 0)--}}
              {{--<div class="alert alert-danger alert-dismissable alert-box">--}}
                {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
                {{--<ul>--}}
                  {{--@foreach ($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                  {{--@endforeach--}}
                {{--</ul>--}}
              {{--</div>--}}
            {{--@endif--}}
        </div>
        @yield('content') 
      </div>
      <footer class="main-footer text-center">
        <strong>Copyright &copy; 2018</strong> All rights reserved. Developed & Maintained by <a href="https://pleximus.com/" target="_blank">Pleximus</a>
      </footer>
    </div>
		<!-- Bootstrap 3.3.6 -->
		<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
		<!-- AdminLTE App -->
		<script src="{{ asset('/js/app.min.js') }}"></script>
		<!-- SlimScroll -->
		<script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- Datepicker JS -->  
		<script src="{{ asset('/js/bootstrap-datepicker.js') }}"></script>
		<!-- Data tables -->
		<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- icheck -->
    <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>  
    <!-- Slick -->
    <script src="{{ asset('/js/slick/slick.min.js') }}"></script> 
		<!-- FastClick -->
		<script src="{{ asset('/plugins/fastclick/fastclick.js') }}"></script>
    <script src="{{ asset('/plugins/select2/select2.full.min.js') }}"></script>  
    <script src="{{ asset('/plugins/chartjs/Chart.min.js') }}"></script>  
    <script src="{{ asset('/js/editor.js') }}"></script>
    <!-- Datarange Picker -->

		<!-- Default JS -->  
		<script src="{{ asset('/js/scheduling.js') }}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    <script>
        $('#demo').daterangepicker({
            "startDate": "03/31/2018",
            "endDate": "04/06/2018"
        }, function(start, end, label) {
            console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {{--<script>--}}

      {{--@if(!empty(session('success')))--}}

      {{--toastr.success('{{session('success')}}')--}}

      {{--@endif--}}
      {{--@if(!empty(session('error')))--}}

      {{--toastr.warning('{{session('error')}}')--}}

      {{--@endif--}}
    {{--</script>endif--}}
	</body>
</html>
