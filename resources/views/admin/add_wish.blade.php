@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add User</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(array('url' => 'wish/save')) !!}

                        @if(!empty($visitor))
                            <input type="hidden" name="wish_id" value="{{$visitor->id}}">              
                        @endif
                        <?php 
                          if(!empty($visitor))
                          {
                            $contacts = explode(' / ', $visitor->contact_number);
                          }
                        ?>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Full Name * </label>
                                <input type="text" name="name" @if(!empty($visitor)) value="{{$visitor->full_name}}" @else value="{{old('name')}}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('name')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Contact Number * </label>
                                <input type="number" name="contact_number"
                                       @if(!empty($visitor)) value="{{ $contacts[0] }}" @else value="{{old('contact_number')}}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('contact_number')}}</span>
                            </div>

                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email</label>
                                <input type="email" name="email" @if(!empty($visitor)) value="{{$visitor->email}}" @else value="{{old('email')}}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('email')}}</span>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Anniversary Date </label>
                                <div class="input-group date any_date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input readonly type="text" class="form-control pull-right" name="any_date" @if(!empty($visitor && $visitor->anniversary_date != "" && $visitor->anniversary_date != '1970-01-01')) value="{{$visitor->anniversary_date}}" @else value="{{old('any_date')}}" @endif>
                                </div>
                                <span class="error-font text-danger">{{ $errors->first('any_date')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Birth Date * </label>
                                <div class="input-group date bdate">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input readonly type="text" class="form-control pull-right" name="bdate" @if(!empty($visitor)) value="{{$visitor->birth_date}}" @else value="{{old('bdate')}}" @endif>
                                </div>
                                <span class="error-font text-danger">{{ $errors->first('bdate')}}</span>
                            </div>

                        </div>
                        <div class="col-md-12">
                          <div class="row">     
                            <div class="col-md-6">                          
                              <div class="form-group">
                                <label for="recipient-name" class="control-label">Contact Number 1 </label>
                                <input type="number" name="contact_number_1"
                                       @if(!empty($visitor)) value="{{ (isset($contacts[1])) ? $contacts[1] : ''}}" @else value="{{old('contact_number_1')}}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('contact_number_1')}}</span>
                              </div>                        
                            </div> 
                            <div class="col-md-6">                          
                                <div class="form-group">
                                  <label for="recipient-name" class="control-label">Contact Number 2 </label>
                                  <input type="number" name="contact_number_2"
                                         @if(!empty($visitor)) value="{{ (isset($contacts[2])) ? $contacts[2] : ''}}" @else value="{{old('contact_number_2')}}"
                                         @endif class="form-control" id="recipient-name">
                                  <span class="error-font text-danger">{{ $errors->first('contact_number_2')}}</span>
                                </div>                        
                            </div> 
                            <div class="col-md-6">                          
                                <div class="form-group">
                                  <label for="recipient-name" class="control-label">Contact Number 3 </label>
                                  <input type="number" name="contact_number_3"
                                         @if(!empty($visitor)) value="{{ (isset($contacts[3])) ? $contacts[3] : ''}}" @else value="{{old('contact_number_3')}}"
                                         @endif class="form-control" id="recipient-name">
                                  <span class="error-font text-danger">{{ $errors->first('contact_number_3')}}</span>
                                </div>                        
                            </div> 
                            <div class="col-md-6">                          
                              <div class="form-group">
                                <label for="recipient-name" class="control-label">Contact Number 4 </label>
                                <input type="number" name="contact_number_4"
                                       @if(!empty($visitor)) value="{{ (isset($contacts[4])) ? $contacts[4] : ''}}" @else value="{{old('contact_number_4')}}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('contact_number_4')}}</span>
                              </div>                        
                            </div>                             
                          </div>
                        </div>
                    </div>

                                <div class="box-footer text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                                {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
    </section>
    <script>
        $( "#type" ).change(function() {
            var val = $(this).val();
            if(val == 'official'){
                $('#department').show();
                $('#department_field').attr('required',true);

            }else if(val == 'general'){
                $('#department').hide();
                $('#department_field').attr('required',false);
            }
        });
    </script>
@endsection
@section('script')

@stop