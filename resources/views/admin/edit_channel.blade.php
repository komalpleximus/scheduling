@extends('layouts.admin_header')
@section('content')
  <section class="content-header">
    <h1>
      Channel
    </h1>
  </section>
  
  <section class="content channel-container">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Channel</h3>
          </div>            
          <form id="form-add-channel" role="form" method="POST" action="{{ url('/channel/edit') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group col-md-12">
                  <input type="hidden" name="channel-id" value="{{ $channel->id }}">
                  <label for="channel">Channel Name</label>
                  <input type="text" class="form-control" id="channel" placeholder="Enter channel name" name="channel" value="{{ $channel->channel }}">
                  <span class="error-font text-danger">{{ $errors->first('channel')}}</span>
                </div>
                <div class="form-group col-md-12">
                  <label>Description</label>
                  <textarea name="description" class="form-control" rows="5" placeholder="Enter Description In Short">{{ $channel->description }}</textarea>
                  <span class="error-font text-danger">{{ $errors->first('description')}}</span>
                </div> 
              </div>   
              <div class="col-md-6">
                <div class="form-group col-md-12">
                  <label for="contact-person">Contact Person Name</label>
                  <input type="text" class="form-control" id="contact-person" placeholder="Enter contact person name" name="contact-person" value="{{ $channel->contact_person }}">
                  <span class="error-font text-danger">{{ $errors->first('contact-person')}}</span>
                </div>
                <div class="form-group col-md-12">
                  <label for="contact-person">Contact Person Designation</label>
                  <input type="text" class="form-control" id="contact-person-designation" placeholder="Enter contact person designation" name="contact-person-designation" value="{{ $channel->contact_person_designation }}">
                  <span class="error-font text-danger">{{ $errors->first('contact-person-designation')}}</span>
                </div>
                <div class="form-group col-md-12">
                  <label for="contact-person">Contact Person Number</label>
                  <input type="text" class="form-control" id="contact-person-number" placeholder="Enter contact person number" name="contact-person-number" value="{{ $channel->contact_person_numbers }}">
                  <span class="error-font text-danger">{{ $errors->first('contact-person-number')}}</span>
                </div>
              </div>
              <div class="form-group col-md-6"> <br>         
                <label for="status">Status&nbsp;&nbsp;&nbsp;</label>
                <label class="radio-inline">
                  <input id="status_1" type="radio" class="minimal status" name="status" value="0"<?php if($channel->status == "0") {echo "checked";}?>>&nbsp;Inactive
                </label>
                <label class="radio-inline">
                  <input id="status_2" type="radio" class="minimal status" name="status" value="1"<?php if($channel->status == "1") {echo "checked";}?>>&nbsp;Activate
                </label>
              </div>            
            </div>           
            <div class="box-footer text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>  
        </div>
      </div>
    </div>
  </section>
  <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  </script>
@stop