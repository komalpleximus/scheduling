@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Appointments</h3>
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(array('url' => 'appointments')) !!}
                                <div class="col-md-3 col-sm-3">

                                    <div class="input-group date ">

                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" readonly id="date_renger" class="form-control pull-right"   name="filter_date" >
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-3">

                                    <select name="type" class="form-control" id="">
                                        <option value="">All</option>
                                        <option @if(!empty($type) && $type == 'no') selected @endif value="no">General</option>
                                        <option @if(!empty($type) && $type == 'Yes') selected @endif value="Yes">Important</option>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="submit" class="btn btn-primary">Filter</button>
                                </div>
                                {!! Form::close() !!}
                                <table id="table-appoinment" style="width:100%" class="table table-bordered table-striped">
                                    <thead>
                                    <tr style="height: 45px">
                                        {{--<th class="text-center">Date Added</th>--}}
                                        <th class="text-center">Appointment Date</th>
                                        <th class="text-center">Appointment Time </th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">Purpose</th>
                                        <th class="text-center">Contact NO.</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($appointments as $appointment)

                                        <tr style="height: 35px">
                                            {{--<td class="text-center">{{ date('d-m-Y h:i', strtotime($appointment->created_at)) }}</td>--}}
                                            <td class="text-center"> {{date('d M Y',strtotime($appointment->appointment_date))}}</td>
                                            <td class="text-center"> {{$appointment->appointment_time}}</td>
                                            <td class="text-center">{{$appointment->full_name}}</td>

                                            @if($appointment->important == 'Yes')
                                            <td class="text-center">Important</td>
                                                @else
                                                <td class="text-center">General</td>
                                            @endif
                                            <td class="text-center">{{$appointment->purpose_of_visit}}</td>
                                            <td class="text-center"> {{$appointment->contact_number}}</td>
                                            <td class="text-center">{{$appointment->email}}</td>

                                            <td width="10%">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{url('appointment/view').'?id='.$appointment->id}}">View</a></li>
                                                        <li><a href="{{url('appointment/edit').'?id='.$appointment->id}}">Edit</a></li>
                                                        <li><a href="#" data-toggle="modal" data-target=".delete_{{$appointment->id}}">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </td>




                                        </tr>

                                        <!-- ========== DELETE MODEL START ========== -->
                                        <div class="modal fade delete_{{$appointment->id}}" id="delete-channel-modal "  role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-md modal-teal">
                                                <div class="modal-content">
                                                    <form id="form-delete-channel" role="form" method="POST" action="{{ url('/appoinment/delete') }}" novalidate>
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h2 class="modal-title text-center color-skyblue">Appointment</h2>
                                                        </div>
                                                        <input type="hidden" name="id" value="{{$appointment->id}}">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-10 col-md-offset-1">
                                                                    <div class="text-center">
                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                        <input type="hidden" id="channel-id" name="channel-id">
                                                                        <h4>Are you sure to delete this appointment?</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="row">
                                                                <div class="col-md-8 col-md-offset-4">
                                                                    <input type="submit" class="btn btn-primary" value="Yes">
                                                                    <a href="/" class="btn btn-primary" data-dismiss="modal">No</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- ========== DELETE MODEL END ========== -->

                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div id="page-link-wrapper" class="col-md-12 text-center"></div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
    <script src="{{asset('/date-range/moment.js')}}" ></script>

    <script>
      $('#date_renger').daterangepicker({
        locale: {
          format: 'DD/MM/YYYY'
        },
        "startDate": "{{$start_date}}",
        "endDate": "{{$end_date}}",
      }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY') + ' (predefined range: ' + label + ')");
      });
    </script>
    <script type="text/javascript">
        $(function(){
            $('#table-appoinment').dataTable( {
                "bLengthChange": false,
                "iDisplayLength": 6,
                "infoEmpty": "<center><div class='text-info'><br>No appoinment available</div></center>",
                "oLanguage": {
                    "sEmptyTable":"<center><div class='text-info'><br>No appoinment available</div></center>",
                    "sSearch": "",
                    "oPaginate": {
                        "sNext": '>',
                        "sLast": '>|',
                        "sFirst": '|<',
                        "sPrevious": '<'
                    }
                },
                "bSort" : false
            });
            $('.dataTables_filter input').attr("placeholder", "Search");
            $('.dataTables_filter input').removeClass("input-sm");
            $('.dataTables_filter input').addClass("form-control");
            $("#table-channel_info").detach().appendTo('#page-link-wrapper');
            $("#table-channel_paginate").detach().appendTo('#page-link-wrapper');
            $("#table-channel_filter").detach().appendTo('#search-channel');

            //Delete channel
            $(document).on('click', '.delete-channel-link', function(ev) {
                ev.preventDefault();
                var channel_id = $(this).data('channel-id');
                $('#channel-id').val(channel_id);
                $('#delete-channel-modal').modal('show');
            });
        });
    </script>
@endsection