@extends('layouts.admin_header')
@section('content')
  <section class="content-header">
    <h1>
      Channel
    </h1>
  </section>
  
  <section class="content channel-container">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Add Channel</h3>
          </div>            
          <form id="form-add-channel" role="form" method="POST" action="{{ url('/channel/add') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group col-md-12">
                  <label for="channel">Channel Name</label>
                  <input type="text" class="form-control" id="channel" placeholder="Enter channel name" name="channel" value="{{ old('channel') }}">
                  <span class="error-font text-danger">{{ $errors->first('channel')}}</span>
                </div>
                <div class="form-group col-md-12">
                  <label>Description</label>
                  <textarea name="description" class="form-control" rows="5" placeholder="Enter Description In Short">{{ old('description') }}</textarea>
                  <span class="error-font text-danger">{{ $errors->first('description')}}</span>
                </div> 
              </div>   
              <div class="col-md-6">
                <div class="form-group col-md-12">
                  <label for="contact-person">Contact Person Name</label>
                  <input type="text" class="form-control" id="contact-person" placeholder="Enter contact person name" name="contact-person" value="{{ old('contact-person') }}">
                  <span class="error-font text-danger">{{ $errors->first('contact-person')}}</span>
                </div>
                <div class="form-group col-md-12">
                  <label for="contact-person">Contact Person Designation</label>
                  <input type="text" class="form-control" id="contact-person-designation" placeholder="Enter contact person designation" name="contact-person-designation" value="{{ old('contact-person-designation') }}">
                  <span class="error-font text-danger">{{ $errors->first('contact-person-designation')}}</span>
                </div>
                <div class="form-group col-md-12">
                  <label for="contact-person">Contact Person Number</label>
                  <input type="text" class="form-control" id="contact-person-number" placeholder="Enter contact person number" name="contact-person-number" value="{{ old('contact-person-number') }}">
                  <span class="error-font text-danger">{{ $errors->first('contact-person-number')}}</span>
                </div>
              </div>               
            </div>           
            <div class="box-footer text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>  
        </div>
      </div>
    </div>
  </section>
@stop