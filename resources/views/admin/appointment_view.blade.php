

@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Appointment Details</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><label style="width:160px; font-weight: 500;">Appointment date </label><b>: {{$appointment->appointment_date}}</b></h4>
                            </div>
                            <div class="col-md-12">
                                <h4><label style="width:160px; font-weight: 500;">Appointment Time </label><b>: {{$appointment->appointment_time}}</b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <table id="table-visitor" style="width:100%" class="table table-bordered table-striped">

                                    <tbody>
                                    <tr>
                                        <th class="text-left" style="width:25%"> Full Name</th>
                                        <td class="text-left"> {{$appointment->full_name}}</td>
                                    </tr>


                                    <tr>
                                        <th class="text-left">Contact</th>
                                        <td class="text-left">{{$appointment->contact_number}}</td>

                                    </tr>
                                    <tr>
                                        <th class="text-left">Email</th>
                                        <td class="text-left">{{$appointment->email}}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-left">Purpose of Visit</th>
                                        <td class="text-left">{{$appointment->purpose_of_visit}}</td>
                                    </tr>
                                    <tr>
																				<th class="text-left">Description</th>
																				<td class="text-left">{{$appointment->description}}</td>
                                    </tr>
                               
                                    <tr>
																				<th class="text-left">Location</th>
																				<td class="text-left">{{$appointment->location}}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-left">Appointment type</th>
                                        <td class="text-left">
                                            @if($appointment->important == 'Yes')
                                                Important
                                             @else
                                                General
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection