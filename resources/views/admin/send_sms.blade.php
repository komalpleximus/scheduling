@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Contact To Send SMS</h3>      
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-9">
                          <form id="form-import-excel-contact" role="form" method="POST" action="{{ url('/send-sms') }}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="excel-file">Import excel file</label>
                                  <!--label id="csv-file" class="button text-info" ><span><i class="fa fa-file fa-1x text-info"></i>&nbsp;&nbsp;Select File</span--> 
                                    <input type="file" class="btn btn-sm excel-file ml-3" name="excel-file" />
                                  <!--/label--><br>
                                  <span class="error-font text-danger">{{ $errors->first('excel-error')}}</span>
                                </div>   
                              </div>          
                            </div>           
                            <div class="row">
                              <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Import</button>
                              </div>
                            </div>
                          </form>  
                        </div>
                        <div class="col-md-3 text-right">
                          <a href="/images/contacts.xlsx" download="contacts_excel" class="btn btn-sm btn-danger">Download Sample Excel Sheet</a>
                        </div>                        
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Send SMS</h3>      
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-6">
                          <form id="form-import-excel-contact" role="form" method="POST" action="{{ url('/send-sms-text') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                            <div class="row">
                              <!--div class="col-md-12">
                                <div class="form-group">
                                  <label for="title">SMS Title</label>
                                  <input type="text" class="form-control" id="title" placeholder="Enter SMS title" name="title" value="{{ old('title') }}">
                                  <span class="error-font text-danger">{{ $errors->first('title')}}</span>
                                </div>
                              </div-->
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>SMS Text</label>
                                  <textarea name="message" class="form-control" rows="6" placeholder="SMS Text" onKeyUP=check_length(this.form); onKeyDown=check_length(this.form);>{{ old('message') }}</textarea>
                                  <span class="error-font text-danger">{{ $errors->first('message')}}</span>
                                </div>  
                              </div>
                              <div class="col-md-4 col-md-offset-8">                              
                               Total Characters :<b> <input size=1  name="text_num" style="border:none;"></b>
                              </div>
                              <input type="hidden" name="contacts" value="{{ (isset($contacts))? $contacts:old('contacts') }}" />                              
                              <input type="hidden" name="contactnames" value="{{ (isset($contactnames))? $contactnames:old('contactnames') }}" />                              
                            </div>           
                            <div class="row">
                              <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Send SMS</button>
                              </div>
                            </div>
                          </form>  
                        </div>
                        <div class="col-md-3 text-center">
                          <label>Contact List {{ (isset($total_contacts))?  $total_contacts:'' }}</label>
                          <ul class="list-group">
                            @if(isset($contacts))
                              <?php 
                                $array_contacts = json_decode($contacts);
                                $array_contactnames = json_decode($contactnames);
                              ?>
                              @foreach($array_contacts as $key=>$contact)
                                <li class="list-group-item">{{ $array_contactnames[$key] }} - {{ $contact }}</li>
                              @endforeach    
                            @elseif(old('contacts'))
                              <?php 
                                $array_contacts = json_decode(old('contacts'));
                                $array_contactnames = json_decode(old('contactnames'));
                              ?>
                              @foreach($array_contacts as $key=>$contact)
                                <li class="list-group-item">{{ $array_contactnames[$key] }} - {{ $contact }}</li>
                              @endforeach    
                            @endif                                             
                                          
                          </ul>                     
                        </div>                        
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<script language=JavaScript>
 function check_length(sms_form)
  {
    var text_count=sms_form.message.value.length;
    sms_form.text_num.value=text_count;
  }
</script>  
@endsection