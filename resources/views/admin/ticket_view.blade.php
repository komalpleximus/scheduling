@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tracking Details</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                          <div class="col-md-12"> 
                            <h4><label style="width:100px; font-weight: 500;">Tracking ID </label><b>: {{$visitor->tracking_id}}</b></h4>
                          </div> 
                          <div class="col-md-12"> 
                            <h4><label style="width:100px; font-weight: 500;">Date </label><b>: {{date('d-m-Y', strtotime($visitor->date_of_visit))}}</b></h4>
                          </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <table id="table-visitor" style="width:100%" class="table table-bordered table-striped">

                                    <tbody>
                                        <tr>
                                            <th class="text-left" style="width:25%"> Full Name</th>
                                             <td class="text-left"> {{$visitor->full_name}}</td>
                                        </tr>


                                           <tr>
                                               <th class="text-left">Contact</th>
                                                <td class="text-left">{{$visitor->contact_number}}</td>

                                           </tr>
                                        <tr>
                                            <th class="text-left">Email</th>
                                             <td class="text-left">{{$visitor->email}}</td>
                                        </tr>
                                        <tr>
                                            <th class="text-left">Field</th>
                                             <td class="text-left">{{$visitor->department}}</td>
                                        </tr>
                                        <tr>
                                            <th class="text-left">Designation</th>
                                             <td class="text-left">{{$visitor->designition}}</td>
                                        </tr>
                                        <tr>
                                            <th class="text-left">Purpose of Visit</th>
                                             <td class="text-left">{{$visitor->purpose_of_visit}}</td>
                                        </tr>
                                        <tr>

                                        </tr>
                                            <th class="text-left">Description</th>
                                             <td class="text-left">{{$visitor->work_description}}</td>
                                        <tr>
                                            <tr>
                                            <th class="text-left">Open Status</th>
                                            <?php
                                            $now = time(); // or your date as well
                                            $your_date = strtotime($visitor->created_at);
                                            $datediff = $now - $your_date;
                                            $day = round($datediff / (60 * 60 * 24));
                                            $text = '';
                                            if ($day == 0) {
                                                $text = "Today";
                                            } elseif ($day == 1) {
                                                $text = 'Open from '. $day . ' day';
                                            } else {
                                                $text = 'Open from '.$day . ' days';
                                            }
                                            ?>
                                             <td class="text-left"> {{$text}}</td>
                                        </tr>
                                        <tr>
                                            <th class="text-left">Ticket Status</th>
                                             <td class="text-left">{{$visitor->status}}</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Ticket Tracking Details</h3>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12"> 
                    <table  style="width:100%" class="table table-bordered table-striped">
                      <thead style="font-size: 16px">
                        <th class="text-left" style="width:25%">Date</th>
                        <th class="text-left">Comments</th>
                      </thead>
                      <tbody>
                        @if(isset($visitor->getComments[0]))
                          @foreach($visitor->getComments as $comment)
                            <tr>
                              <td class="text-left">{{date('d-m-Y',strtotime($comment->comments_date))}}</td>
                              <td class="text-left">{{$comment->comment}}</td>
                            </tr>
                          @endforeach
                        @else
                        <tr>
                          <td class="text-center" colspan="2">No Comments</td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>

@endsection