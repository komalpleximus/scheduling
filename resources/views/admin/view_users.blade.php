@extends('layouts.admin_header')
@section('content')
  <section class="content user-container">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Users</h3>
            <div id="search-user" class="pull-right"></div>
          </div>
          <div class="box-body">   
            <div class="row">
              <div class="col-md-2">
                  <label>Filter By Created Date</label>
              </div>
              <div class="col-md-3 col-sm-3">                   
                <div class="input-group date ">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" readonly id="date_renger" class="form-control pull-right date_renger" name="filter_date" >
                </div>
              </div>
              <div class="col-md-1">
                <button type="button" class="btn btn-primary filter">Filter</button>
              </div>
            </div>
            <table id="table-users" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%" class="text-center">No.</th>
                  <th>Name</th>
                  <th class="text-center">Birth Date</th>
                  <th class="text-center">Anniversary Date</th>
                  <th class="text-center">Contact Number</th>
                  <th class="text-center">Email</th>
                  <th class="text-center">Created At</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php $i = 1; ?>
                @foreach($users as $user)
                  <tr>
                    <td class="text-center">{{ $i++ }}</td>
                    <td>{{ $user->full_name }}</td>
                    <td class="text-center">@if ($user->birth_date != "" && $user->birth_date != '1970-01-01'){{ date('d F', strtotime($user->birth_date)) }} @endif</td>
                    <td class="text-center">@if ($user->anniversary_date != "" && $user->anniversary_date != '1970-01-01'){{ date('d F', strtotime($user->anniversary_date)) }} @endif</td>
                    <td class="text-center">{{ $user->contact_number }}</td>
                    <td class="text-center">{{ $user->email }}</td>
                    <td class="text-center">{{ date('d-m-Y', strtotime($user->created_at)) }}</td>
                    <td class="text-center">
                      <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('wish/view').'?id='.$user->id}}">View</a></li>
                            <li><a href="{{url('wish/add').'?id='.$user->id}}">Edit</a></li>
                            <li><a href="#" data-toggle="modal" data-target=".delete_{{$user->id}}">Delete</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                   <!-- ========== DELETE MODEL START ========== -->
                      <div class="modal fade delete_{{$user->id}}" id="delete-channel-modal "  role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-md modal-teal">
                              <div class="modal-content">
                                  <form id="form-delete-channel" role="form" method="POST" action="{{ url('/wish/delete') }}" novalidate>
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h2 class="modal-title text-center color-skyblue">User</h2>
                                      </div>
                                      <input type="hidden" name="id" value="{{$user->id}}">
                                      <div class="modal-body">
                                          <div class="row">
                                              <div class="col-md-10 col-md-offset-1">
                                                  <div class="text-center">
                                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                      <input type="hidden" id="channel-id" name="channel-id">
                                                      <h4>Are you sure to delete this user?</h4>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                          <div class="row">
                                              <div class="col-md-8 col-md-offset-4">
                                                  <input type="submit" class="btn btn-primary" value="Yes">
                                                  <a href="/" class="btn btn-primary" data-dismiss="modal">No</a>
                                              </div>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                      <!-- ========== DELETE MODEL END ========== -->
                @endforeach
              </tbody>
            </table>
            <div class="row">
              <div id="page-link-wrapper" class="col-md-12 text-center"></div>
            </div>
          </div>
        </div>
      </div> 
    </div>  
  </section>
  
  <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
  <script src="{{asset('/date-range/moment.js')}}" ></script>
  <script type="text/javascript">
		$(function(){
      $('#date_renger').daterangepicker({
        locale: {
          format: 'DD/MM/YYYY'
        },
        "startDate": "{{$start_date}}",
        "endDate": "{{$end_date}}",
      }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY') + ' (predefined range: ' + label + ')");
      });
        
			$('#table-users').dataTable( {
				"bLengthChange": false,
				"iDisplayLength": 15,
				"infoEmpty": "<center><div class='text-info'><br>No user available</div></center>",
				"oLanguage": {
          "sEmptyTable":"<center><div class='text-info'><br>No user available</div></center>",
          "sSearch": "",
          "oPaginate": {
            "sNext": '>',
            "sLast": '>|',
            "sFirst": '|<',
            "sPrevious": '<'
          }
        },
        "bSort" : false
			});
			$('.dataTables_filter input').attr("placeholder", "Search");
			$('.dataTables_filter input').removeClass("input-sm");
      $('.dataTables_filter input').addClass("form-control");
			$("#table-users_info").detach().appendTo('#page-link-wrapper');
			$("#table-users_paginate").detach().appendTo('#page-link-wrapper');
			$("#table-users_filter").detach().appendTo('#search-user');

      $('.filter').on('click',function(){
        var filter_date = $(".date_renger").val();     
        var type = $('.select-type').val();     
        window.location.href = '/wishes/users?type='+type+'&filter_date='+filter_date;
      }); 
      
      //Delete user
      $(document).on('click', '.delete-user-link', function(ev) {
        ev.preventDefault();
        var user_id = $(this).data('user-id');
        $('#user-id').val(user_id);
        $('#delete-user-modal').modal('show');
      });
		});
	</script>
@endsection