@extends('layouts.admin_header')
@section('content')
  <section class="content user-container">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Upcoming Dates for Wishes</h3>
            <div id="search-user" class="pull-right"></div>
          </div>
          <div class="box-body">   
            <div class="row">
              <div class="col-md-3">
                <select name="type" id="type" class="form-control select-type">
                  <option value="0"      
                  <?php 
                    if(isset($type) && $type == 0)
                    {echo 'selected';} 
                  ?>>All</option>
                  <option value="1"
                  <?php 
                    if(isset($type) && $type == 1)
                    {echo 'selected';} 
                  ?>
                  >Birthday</option>
                  <option value="2"
                  <?php 
                    if(isset($type) && $type == 2)
                    {echo 'selected';} 
                  ?>
                  >Anniversary</option>
                </select>  
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="input-group date ">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" readonly id="date_renger" class="form-control pull-right date_renger" name="filter_date" >
                </div>
              </div>
              <div class="col-md-1">
                <button type="button" class="btn btn-primary filter">Filter</button>
              </div>
            </div>
            <table id="table-users" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%" class="text-center">No.</th>
                  <th>Name</th>
                  <th class="text-center">Wishing Date</th>
                  <th class="text-center">Wishing Type</th>
                  <th class="text-center">Contact Number</th>
                  <th class="text-center">Email</th>
                </tr>
              </thead>
              <tbody>
              <?php $i = 1; ?>
                @foreach($users as $user)
                  <tr>
                    <td class="text-center">{{ $i++ }}</td>
                    <td>{{ $user->full_name }}</td>
                    <td class="text-center">{{ date('d F', strtotime($user->view_date)) }}</td>
                    <td class="text-center">{{ $user->view_type }}</td>
                    <td class="text-center">{{ $user->contact_number }}</td>
                    <td class="text-center">{{ $user->email }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <div class="row">
              <div id="page-link-wrapper" class="col-md-12 text-center"></div>
            </div>
          </div>
        </div>
      </div> 
    </div>  
  </section>
  
  <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
  <script src="{{asset('/date-range/moment.js')}}" ></script>
  <script type="text/javascript">
		$(function(){
      $('#date_renger').daterangepicker({
        locale: {
          format: 'DD/MM/YYYY'
        },
        "startDate": "{{$start_date}}",
        "endDate": "{{$end_date}}",
      }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY') + ' (predefined range: ' + label + ')");
      });
        
			$('#table-users').dataTable( {
				"bLengthChange": false,
				"iDisplayLength": 15,
				"infoEmpty": "<center><div class='text-info'><br>No user available</div></center>",
				"oLanguage": {
          "sEmptyTable":"<center><div class='text-info'><br>No user available</div></center>",
          "sSearch": "",
          "oPaginate": {
            "sNext": '>',
            "sLast": '>|',
            "sFirst": '|<',
            "sPrevious": '<'
          }
        },
        "bSort" : false
			});
			$('.dataTables_filter input').attr("placeholder", "Search");
			$('.dataTables_filter input').removeClass("input-sm");
      $('.dataTables_filter input').addClass("form-control");
			$("#table-users_info").detach().appendTo('#page-link-wrapper');
			$("#table-users_paginate").detach().appendTo('#page-link-wrapper');
			$("#table-users_filter").detach().appendTo('#search-user');

      $('.filter').on('click',function(){
        var filter_date = $(".date_renger").val();     
        var type = $('.select-type').val();    
        window.location.href = '/wishes/history?type='+type+'&filter_date='+filter_date;
      });      

      /*$('#date_renger').on('apply.daterangepicker',function(){
        var filter_date = $(".date_renger").val();     
        var type = $('.select-type').val();     
        window.location.href = '/wishes/history?type='+type+'&filter_date='+filter_date;
      });*/ 
      
      //Delete user
      $(document).on('click', '.delete-user-link', function(ev) {
        ev.preventDefault();
        var user_id = $(this).data('user-id');
        $('#user-id').val(user_id);
        $('#delete-user-modal').modal('show');
      });
		});
	</script>
@endsection