@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Users</h3>      
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-9">
                          <form id="form-import-excel-contact" role="form" method="POST" action="{{ url('/wishes/import') }}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="excel-file">Import excel file</label>
                                  <!--label id="csv-file" class="button text-info" ><span><i class="fa fa-file fa-1x text-info"></i>&nbsp;&nbsp;Select File</span--> 
                                    <input type="file" class="btn btn-sm excel-file ml-3" name="excel-file" />
                                  <!--/label--><br>
                                  <span class="error-font text-danger">{{ $errors->first('excel-error')}}</span>
                                </div>   
                              </div>          
                            </div>           
                            <div class="row">
                              <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Import</button>
                              </div>
                            </div>
                          </form>  
                        </div>
                        <div class="col-md-3 text-right">
                          <a href="/images/user_excel.xlsx" download="user_excel" class="btn btn-sm btn-danger">Download Sample Excel Sheet</a>
                        </div>                        
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection