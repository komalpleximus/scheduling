@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Send Mail</h3>      
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
											<div class="row">
                        <div class="col-md-12">
                          <form id="form-file-upload" role="form" method="POST" action="{{ url('/file-upload') }}" enctype="multipart/form-data">
                            <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}"> 
              							<div class="row">
															<div class="col-md-6 form-group upload-image">
																<label>Upload Image</label>
																<input id="image" type="file" name="image" class="form-control" />
															</div>
															<div class="col-md-6">
															  <label>Click to upload image</label><br>
                                <button type="submit" class="btn btn-primary">Upload</button>
                              </div>
														</div>						
														<div class="row">
															<div class="col-md-10">
																<div class="input-group">
																	 <input type="text" id="image-url" class="form-control" value="" aria-describedby="copy-addon" readonly>
																	 <span class="input-group-addon" id="copy-addon" data-clipboard-target="#image-url">Copy!</span>
																 </div>
															</div>
														</div>     
                          </form>  
                        </div>                              
                      </div><hr>
                      <div class="row">
                        <div class="col-md-6">
                          <form id="form-send-email" role="form" method="POST" action="{{ url('/send-mail-text') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                            <input type="hidden" name="group-id" value="{{ $interest_group_id }}"> 
                            <input type="hidden" name="list-id" value="{{ $list_id }}"> 
                            <div class="row form-group">
															<div class="col-md-12 from-group">
																<label class="col-md-4">
																	<input value="0" name="all-groups" type="checkbox">&nbsp;&nbsp;All
																</label>
															@foreach($groups as $group_id => $group)
																	<label class="col-md-4">
																		<input value="{{ $group_id }}" class="groups" name="group[]" type="checkbox">&nbsp;&nbsp;{{ $group }}
																	</label>
																@endforeach
															</div>
														</div>
                            <div class="row form-group">
															<div class="col-md-12">
																<label>Subject</label>
																<input name="subject" class="form-control" />
															</div>
														</div>
                            <div class="row">
                              <!--div class="col-md-12">
                                <div class="form-group">
                                  <label for="title">Mail Title</label>
                                  <input type="text" class="form-control" id="title" placeholder="Enter Mail title" name="title" value="{{ old('title') }}">
                                  <span class="error-font text-danger">{{ $errors->first('title')}}</span>
                                </div>
                              </div-->
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>Mail Text</label>
																	<div id="txt-editor"></div> 
																	<input id="message" name="message" type="hidden" />     
																	<span class="error-font text-danger">{{ $errors->first('message')}}</span>
                                </div>  
                              </div>
                              <div class="col-md-4 col-md-offset-8">                              
                               Total Characters :<b> <input size=1  name="text_num" style="border:none;"></b>
                              </div>
                              <input type="hidden" name="emails" value="{{ (isset($emails))? $emails:old('emails') }}" />                              
                              <input type="hidden" name="emailnames" value="{{ (isset($emailnames))? $emailnames:old('emailnames') }}" />                              
                            </div>           
                            <div class="row">
                              <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Send Mail</button>
                              </div>
                            </div>
                          </form>  
                        </div>
                        <div class="col-md-6">
                          <label>Email List ({{ count($members) > 0 ?  count($members) : '0' }})</label>
													<div class="">
														<table class="table" id="table-users">
															<thead>
																<th>Name</th>
																<th>Email</th>
																<th>Group</th>
															</thead>
															<tbody>
																@foreach($members as $email => $member)
																	<tr>
																		<td>{{ $member['fname'].' '.$member['lname'] }}</td>
																		<td>{{ $email }}</td>
																		<td>
																			@foreach($member['groups'] as $group_id => $member_group)
																				@if($member_group == true && isset($groups[$group_id]))
																					<span class="label-primary">&nbsp;&nbsp;{{ $groups[$group_id] }}&nbsp;&nbsp;</span>
																				@endif
																			@endforeach
																		</td>
																	</tr>
																@endforeach
															</tbody>
														</table>
													</div>
                          <ul class="list-group">
                            @if(isset($emails))
                              <?php 
                                $array_emails = json_decode($emails);
                                $array_emailnames = json_decode($emailnames);
                              ?>
                              @foreach($array_emails as $key=>$email)
                                
                              @endforeach    
                            @elseif(old('emails'))
                              <?php 
                                $array_emails = json_decode(old('emails'));
                                $array_emailnames = json_decode(old('emailnames'));
                              ?>
                              @foreach($array_emails as $key=>$email)
                                <li class="list-group-item">{{ $array_emailnames[$key] }} - {{ $email }}</li>
                              @endforeach    
                            @endif
                          </ul>                     
                        </div>  
                        <!--div class="col-md-3 text-center">
                          <label>Email List {{ (isset($total_emails))?  $total_emails:'' }}</label>
                          <ul class="list-group">
                            @if(isset($emails))
                              <?php 
                                $array_emails = json_decode($emails);
                                $array_emailnames = json_decode($emailnames);
                              ?>
                              @foreach($array_emails as $key=>$email)
                                <li class="list-group-item">{{ $array_emailnames[$key] }} - {{ $email }}</li>
                              @endforeach    
                            @elseif(old('emails'))
                              <?php 
                                $array_emails = json_decode(old('emails'));
                                $array_emailnames = json_decode(old('emailnames'));
                              ?>
                              @foreach($array_emails as $key=>$email)
                                <li class="list-group-item">{{ $array_emailnames[$key] }} - {{ $email }}</li>
                              @endforeach    
                            @endif                                             
                                          
                          </ul>                     
                        </div-->                        
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
                   
                                
 <script src="{{asset('/js/clipboard.min.js')}}"></script>
 <script>
   var clip = new Clipboard('.input-group-addon');
 </script>
		
<script language=JavaScript>
 function check_length(mail_form)
  {
    var text_count=mail_form.message.value.length;
    mail_form.text_num.value=text_count;
  }
</script>

<script type="text/javascript">
    $(document).ready( function() {
			
		//Upload Image
		$("#form-file-upload").submit(function(ev){
			ev.preventDefault();	
			$(this).find(".text-danger").remove();
			$(this).find(".has-error").removeClass("has-error");
			var formURL = $(this).attr("action");
			var postData = $(this).serializeArray(); 
			var file_data = $('#image').prop('files')[0];   
			var token = $('#token').val();   
			var form_data = new FormData();                  
			form_data.append('image', file_data);
			form_data.append('_token', token);
			$.ajax({
				url: formURL,
				type: 'POST',
				enctype: 'multipart/form-data',
				processData: false,  // Important!
        contentType: false,
				cache: false,
				data: form_data,
				success: function(data, textStatus, jqXHR){
					//console.log(data);
					$("#image-url").val(data);
					//console.log($("#image-url").val());
				},
				error: function(jqXHR, textStatus, errorThrown){
					var errResponse = JSON.parse(jqXHR.responseText);
					if (errResponse.error) {
						$.each(errResponse.error, function(index, value)
						{ 	
							if (value.length != 0)
							{
								var $inpElm = $("#" + index);
								$inpElm.closest('.form-group').addClass('has-error');
								$inpElm.closest('.form-group').append('<span class="text-danger">' + value + '</span>');
							}
						});
					}
				},
			});
		});	
	
			var table = $('#table-users').DataTable( {
				"bLengthChange": false,
				"iDisplayLength": 15,
				"infoEmpty": "<center><div class='text-info'><br>No user available</div></center>",
				"oLanguage": {
          "sEmptyTable":"<center><div class='text-info'><br>No user available</div></center>",
          "sSearch": "",
          "oPaginate": {
            "sNext": '>',
            "sLast": '>|',
            "sFirst": '|<',
            "sPrevious": '<'
          }
        },
        "bSort" : false
			});
			$('.dataTables_filter input').attr("placeholder", "Search");
			$('.dataTables_filter input').removeClass("input-sm");
      $('.dataTables_filter input').addClass("form-control");
			//$("#table-users_info").detach().appendTo('#page-link-wrapper');
			//$("#table-users_paginate").detach().appendTo('#page-link-wrapper');
			//$("#table-users_filter").detach().appendTo('#search-user');
			
			$('input').on('ifChecked', function(event){
				if(!$(this).hasClass('groups')){
					$("input").iCheck("check");
					//table.search('').draw();
					$('#table-users_filter input').val('');
				}
				else{
					var search_str = '';
					$('.groups:checkbox:checked').each(function () {
						search_str += ' '+$.trim($(this).closest('label').text());
					});
					//table.search(search_str).draw();
				}
			});

			$('input').on('ifUnchecked', function(event){
				$("input[name=all-groups]").iCheck("uncheck");
					var search_str = '';
					$('.groups:checkbox:checked').each(function () {
						search_str += ' '+$.trim($(this).closest('label').text());
					});
					//table.search(search_str).draw();
			});
			
      $("#txt-editor").Editor({
        'insert_table':false,
        'insert_img':true,
        'print':false,
        //'fonts':false,
        //'styles':false,
        //'font_size':false,
        //'l_align':false,
        //'r_align':false,
        //'c_align':false,
        //'justify':false,
        //'indent':false,
        //'outdent':false,
        'block_quote':false,
        'strikeout':false,
        'hr_line':false,
        'splchars':false,
        'source':false,
      });
	  
	   $("#txt-editor").Editor("setText", '<?php echo old('message');?>');
		$("#form-send-email").submit(function(){
			$("#message").val($("#txt-editor").Editor("getText"));
		});
	  
	});
  </script>	  
@endsection