@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Email Signature</h3>      
                        <div id="search-appointment" class="pull-right"></div>
                    </div>
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-6">
                          <form id="form-send-email" role="form" method="POST" action="{{ url('/email-signature') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
																	<div id="txt-editor"></div> 
																	<input id="message" name="message" type="hidden" />     
																	<span class="error-font text-danger">{{ $errors->first('message')}}</span>
                                </div>  
                              </div>                           
                            </div>           
                            <div class="row">
                              <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Update</button>
                              </div>
                            </div>
                          </form>  
                        </div>                       
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 

<script type="text/javascript">
	$(document).ready( function() {
		
		$("#txt-editor").Editor({
			'insert_table':false,
			'insert_img':true,
			'print':false,
			//'fonts':false,
			//'styles':false,
			//'font_size':false,
			//'l_align':false,
			//'r_align':false,
			//'c_align':false,
			'justify':false,
			'indent':false,
			'outdent':false,
			'block_quote':false,
			'strikeout':false,
			'hr_line':false,
			'splchars':false,
			'source':true,
		});
	
		$("#txt-editor").Editor("setText", '{!! $signature !!}');
		$("#form-send-email").submit(function(){
			$("#message").val($("#txt-editor").Editor("getText"));
		});
	  
	});
  </script>	  
@endsection