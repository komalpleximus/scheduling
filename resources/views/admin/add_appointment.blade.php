@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">@if(!empty($appointment)) Edit @else Add @endif Appointment</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(array('url' => 'appointment/save')) !!}

                        @if(!empty($appointment))
                            <input type="hidden" name="appointment_id" value="{{$appointment->id}}">
                        @endif

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Appointment Date * </label>
                                <div class="input-group date appointment_date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input readonly type="text" class="form-control pull-right"  name="appointment_date" @if(!empty($appointment)) value="{{$appointment->appointment_date}}" @else value="{{ old('appointment_date') }}"  @endif>
                                </div>
                                <span class="error-font text-danger">{{ $errors->first('appointment_date')}}</span>
                            </div>
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>Appointment Time * </label>

                                    <div class="input-group">
                                        <input type="text" name="appointment_time" @if(!empty($appointment)) value="{{$appointment->appointment_time}}" @else  value="{{ old('appointment_time') }}" @endif class="form-control timepicker">

                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                    <span class="error-font text-danger">{{ $errors->first('appointment_time')}}</span>
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Full Name * </label>
                                <input type="text" name="name" @if(!empty($appointment)) value="{{$appointment->full_name}}"  @else value="{{ old('name') }}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('name')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Contact Number * </label>
                                <input type="number" name="contact_number"
                                       @if(!empty($appointment)) value="{{$appointment->contact_number}}"  @else value="{{ old('contact_number') }}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('contact_number')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email </label>
                                <input type="email" name="email" @if(!empty($appointment)) value="{{$appointment->email}}"   @else value="{{ old('email') }}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('email')}}</span>
                            </div>

                            

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Purpose of Visit * </label>
                                <input type="text" name="purpose_of_visit"
                                       @if(!empty($appointment)) value="{{$appointment->purpose_of_visit}}"  @else value="{{ old('purpose_of_visit') }}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('purpose_of_visit')}}</span>
                            </div>
														<div class="form-group">
                                <label for="location" class="control-label">Location * </label>
                                <input type="text" name="location" @if(!empty($appointment)) value="{{$appointment->location}}"  @else value="{{ old('location') }}"
                                       @endif class="form-control" id="recipient-name">
                                <span class="error-font text-danger">{{ $errors->first('location')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Description * </label>
                                @if(!empty($appointment))
                                    <textarea name="description" class="form-control" id="" cols="30"
                                              rows="5">{!! $appointment->description !!}</textarea>
                                    <span class="error-font text-danger">{{ $errors->first('description')}}</span>
                                @else
                                    <textarea name="description" class="form-control" id="" cols="30"
                                              rows="5">{{ old('description') }}</textarea>
                                    <span class="error-font text-danger">{{ $errors->first('description')}}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Is Important * </label>
                                <select name="important" id="type" class="form-control">
                                    @if(!empty($appointment))
                                        <option value="Yes" @if($appointment->important == 'Yes') selected @endif >
                                            Yes
                                        </option>
                                        <option value="no" @if($appointment->important == 'No') selected @endif>
                                            No
                                        </option>
                                    @else
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    @endif
                                </select>
                                <span class="error-font text-danger">{{ $errors->first('important')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
    </section>
    <!-- bootstrap time picker -->
    <script src="{{ asset('/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script>
        $( "#type" ).change(function() {
            var val = $(this).val();
            if(val == 'official'){
                $('#department').show();
                $('#department_field').attr('required',true);

            }else if(val == 'general'){
                $('#department').hide();
                $('#department_field').attr('required',false);
            }
        });
        
        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
    </script>
@endsection
@section('script')

@stop