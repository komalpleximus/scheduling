@extends('layouts.admin_header')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ticket Tracking Details</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(array('url' => 'update/ticket')) !!}

                        @if(!empty($visitor))
                            <input type="hidden" name="visitor_id" value="{{$visitor->id}}">
                        @endif


                        <div class="col-md-6">
                            <table style="width:100%" class="table table-bordered table-striped">
                                <thead>
                                <tr style="height: 45px">
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Comment</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($visitor->getComments[0]))
                                    @foreach($visitor->getComments as $comment)

                                        <tr style="height: 35px">
                                            <td>{{$comment->comments_date}}</td>
                                            <td>{{$comment->comment}}</td>
                                        </tr>

                                    @endforeach
                                @else
                                    <tr style="height: 45px">
                                        <td colspan="2" style="text-align: center">No Comments available</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Status </label>

                                <select name="status" id="type" class="form-control">
                                    @if(!empty($visitor))

                                        <option value="In progress"
                                                @if($visitor->status == 'In progress') selected @endif >
                                            In progress
                                        </option>
                                        <option value="Hold" @if($visitor->status == 'Hold') selected @endif>
                                            Hold
                                        </option>
                                        <option value="Closed" @if($visitor->status == 'Closed') selected @endif>
                                            Closed
                                        </option>
                                    @else

                                    @endif
                                </select>
                                <span class="error-font text-danger">{{ $errors->first('name')}}</span>
                            </div>

                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Date *</label>
                                <div class="input-group date bdate">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input readonly type="text" class="form-control pull-right" name="comments_date"
                                           value="">
                                </div>
                                <span class="error-font text-danger">{{ $errors->first('comments_date')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Your Comment *</label>
                                @if(!empty($visitor))
                                    <textarea name="comment" class="form-control" id="" cols="30"
                                              rows="10"></textarea>
                                    <span class="error-font text-danger">{{ $errors->first('comment')}}</span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
    </section>

@endsection
@section('script')

@stop