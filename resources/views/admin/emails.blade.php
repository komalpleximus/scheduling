@extends('layouts.admin_header')
@section('content')
  <section class="content user-container">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Emails</h3>
            <div id="search-user" class="pull-right"></div>
          </div>
          <div class="box-body">
						<table id="table-users" class="table table-bordered table-striped">
							<thead>
								<th class="hide">No.</th>
								<th>Title</th>
								<th>Description</th>
								<th>Date</th>
								<th>Recipients</th>
								<th>Emails Sent</th>
								<th>Unique Opens</th>
								<th>Open Rate</th>
							</thead>
							<tbody>
								<?php $i = 1; ?>
								@foreach($campaigns as $campaign)
									@if(!isset($campaign->settings->subject_line) || (isset($campaign->settings->subject_line) && $campaign->settings->subject_line == ''))
											<?php continue; ?>
									@endif
									<tr>
										<td class="hide">{{ $i++ }}</td>
										<td>{{ isset($campaign->settings->subject_line) ? $campaign->settings->subject_line : '' }}</td>
										<td>{!! isset($campaign->recipients->segment_text) ? $campaign->recipients->segment_text : '' !!}</td>
										<td data-order="{{  strtotime($campaign->create_time) }}">{{ isset($campaign->create_time) ? date('d/m/Y', strtotime($campaign->create_time)) : '' }}</td>
										<td>{{ isset($campaign->recipients->recipient_count) ? $campaign->recipients->recipient_count : '' }}</td>
										<td>{{ isset($campaign->emails_sent) ? $campaign->emails_sent : '' }}</td>
										<td>{{ isset($campaign->report_summary->unique_opens) ? $campaign->report_summary->unique_opens : '' }}</td>
										<td>{{ isset($campaign->report_summary->open_rate) ? ($campaign->report_summary->open_rate * 100).'%' : '0%' }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
            <div class="row">
              <div id="page-link-wrapper" class="col-md-12 text-center"></div>
            </div>
          </div>
        </div>
      </div> 
    </div>  
  </section>
  
  <script src="{{asset('/date-range/daterangepicker.js')}}" ></script>
  <script src="{{asset('/date-range/moment.js')}}" ></script>
  <script type="text/javascript">
		$(function(){
			$('#table-users').dataTable( {
				"bLengthChange": false,
				"iDisplayLength": 15,
				"infoEmpty": "<center><div class='text-info'><br>No user available</div></center>",
				"oLanguage": {
          "sEmptyTable":"<center><div class='text-info'><br>No user available</div></center>",
          "sSearch": "",
          "oPaginate": {
            "sNext": '>',
            "sLast": '>|',
            "sFirst": '|<',
            "sPrevious": '<'
          }
        },
        "bSort" : true,
				"order": [[ 3, "desc" ]],
			});
			$('.dataTables_filter input').attr("placeholder", "Search");
			$('.dataTables_filter input').removeClass("input-sm");
      $('.dataTables_filter input').addClass("form-control");
			$("#table-users_info").detach().appendTo('#page-link-wrapper');
			$("#table-users_paginate").detach().appendTo('#page-link-wrapper');
			$("#table-users_filter").detach().appendTo('#search-user');

		});
	</script>
@endsection