@extends('layouts.admin_header')
@section('content')
  <section class="content channel-container">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Channels</h3>
            <div id="search-channel" class="pull-right"></div>
          </div>
          <div class="box-body">
            <table id="table-channel" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="5%" class="text-center">No.</th>
                  <th>Name</th>
                  <th class="text-center">Desription</th>
                  <th class="text-center">Contact</th>
                  <th class="text-center">Status</th>
                  <th width="10%" class="text-center">Edit</th>
                  <th width="10%" class="text-center">Delete</th>
                </tr>
              </thead>
              <tbody>
              <?php $i = 1; ?>
                @foreach($channels as $channel)
                  <tr>
                    <td class="text-center">{{ $i++ }}</td>
                    <td>{{ $channel->channel }}</td>
                    <td>{{ $channel->description }}</td>
                    <td>{{ $channel->contact_person }}, {{ $channel->contact_person_designation }}, {{ $channel->contact_person_numbers }}</td>
                    @if($channel->status == 0)
                      <td>Inactive</td>
                    @else
                      <td>Active</td>
                    @endif
                    <td width="10%" class="text-center">
                      <a href="/channel/edit?channel-id={{ $channel->id }}" type="button" class="btn label label-primary">Edit</a>
                    </td>             
                    <td width="10%" class="text-center">  
                      <a type="button" class="btn label label-danger delete-channel-link" data-channel-id="{{ $channel->id }}">Delete</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <div class="row">
              <div id="page-link-wrapper" class="col-md-12 text-center"></div>
            </div>
          </div>
        </div>
      </div> 
    </div>

    
    <!-- ========== DELETE MODEL START ========== -->  
    <div class="modal fade" id="delete-channel-modal" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md modal-teal">
        <div class="modal-content">
          <form id="form-delete-channel" role="form" method="POST" action="{{ url('/channel/delete') }}" novalidate>
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h2 class="modal-title text-center color-skyblue">Channel</h2>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="text-center">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">               
                    <input type="hidden" id="channel-id" name="channel-id">
                    <h4>Are you sure to delete this channel?</h4>
                  </div>
                </div>
              </div>      
            </div>
            <div class="modal-footer">
              <div class="row">
                <div class="col-md-8 col-md-offset-4">
                  <input type="submit" class="btn btn-primary" value="Yes">
                  <a href="/" class="btn btn-primary" data-dismiss="modal">No</a> 
                </div>
              </div>            
            </div>
          </form>
        </div>
      </div> 
    </div>
    <!-- ========== DELETE MODEL END ========== -->
  </section>
  <script type="text/javascript">
		$(function(){
			$('#table-channel').dataTable( {
				"bLengthChange": false,
				"iDisplayLength": 6,
				"infoEmpty": "<center><div class='text-info'><br>No channel available</div></center>",
				"oLanguage": {
          "sEmptyTable":"<center><div class='text-info'><br>No channel available</div></center>",
          "sSearch": "",
          "oPaginate": {
            "sNext": '>',
            "sLast": '>|',
            "sFirst": '|<',
            "sPrevious": '<'
          }
        },
        "bSort" : false
			});
			$('.dataTables_filter input').attr("placeholder", "Search");
			$('.dataTables_filter input').removeClass("input-sm");
      $('.dataTables_filter input').addClass("form-control");
			$("#table-channel_info").detach().appendTo('#page-link-wrapper');
			$("#table-channel_paginate").detach().appendTo('#page-link-wrapper');
			$("#table-channel_filter").detach().appendTo('#search-channel');

      //Delete channel
      $(document).on('click', '.delete-channel-link', function(ev) {
        ev.preventDefault();
        var channel_id = $(this).data('channel-id');
        $('#channel-id').val(channel_id);
        $('#delete-channel-modal').modal('show');
      });
		});
	</script>
@endsection