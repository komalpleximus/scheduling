<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailTables extends Migration
{
	public function up() {
		Schema::create('user_emails', function (Blueprint $table) {
				$table->increments('id');
				$table->string('name');
				$table->string('email');
				$table->string('groups');
				$table->timestamps();
		});
	}

	public function down() {
		Schema::drop('user_emails');
	}
}
