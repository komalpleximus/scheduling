<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $data = array(
    ['id' => 1, 'name' => 'Pleximus', 'email' => 'admin@pleximus.com', 'number' => '9892078909',
    'password' => '$2y$10$5XHJpL4h.LqexidRczCuZ.1rm5lLirETMZgZ75Z9q.xGDKhaZmanW']); //Eu7D03BFxT
    
    DB::table('users')->insert($data);
  }
}
