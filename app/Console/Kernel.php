<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\SendTodayWishes::class,
        Commands\SendTomorrowWishes::class,
        Commands\SendTodayAppointmentList::class,
        Commands\SendTomorrowAppointmentList::class,
        Commands\SendWishes::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();  
        $schedule->command('sendtodaywishes')
                 ->hourly();
        $schedule->command('sendtomorrowwishes')
                 ->hourly();
        $schedule->command('sendtodayappointmentlist')
                 ->hourly();
        $schedule->command('sendtomorrowappointmentlist')
                 ->hourly();
        $schedule->command('sendwishes')
                 ->hourly();
    }
}
