<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\CronController;

class SendTomorrowWishes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //command: php artisan sendtomorrowwishes
    protected $signature = 'sendtomorrowwishes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to send tomorrow wishes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CronController $cron_controller)
    {
        parent::__construct();
        $this->cron_controller = $cron_controller;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->cron_controller->SendTomorrowWishes();
      $this->line("Tomorrow Wishes Send Successfully.");  
    }
}
