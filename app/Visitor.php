<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\trackingComent;

class Visitor extends Model
{
    public function getComments(){
        return $this->hasMany('App\trackingComent');
    }
}
