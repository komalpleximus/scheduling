<?php

namespace App\Http\Controllers;

use App\Wish;
use Illuminate\Http\Request;

use DB;
use Input;
use Excel;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Validator;

class WishController extends Controller
{
    public function __construct(HomeController $home_controller)
    {
      $this->home_controller = $home_controller;
      $this->sms_balance = $this->home_controller->getSMSBalance();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wishAdd(Request $request)
    {
        $wish = '';
        if(isset($request->id)){
            $wish = Wish::find($request->id);
        }


        return view('admin.add_wish')->with('menu', 'wishes')->with('sub_menu', 'add_wishes')->with('visitor',$wish)->with('sms_balance', $this->sms_balance);
    }

    public function wishSave(Request $request){

        try{
            $request_data = $request->all();
//            $email = $request_data['user_email'];

            $messages = [

                'name.required' => 'Please enter full name',
                'contact_number.required' => 'Please enter contact number',
                'contact_number.numeric' => 'Contact number must be numeric',
                'contact_number.regex' => 'Contact number must be 10 digits',
                'contact_number_1.regex' => 'Contact number must be 10 digits',
                'contact_number_2.regex' => 'Contact number must be 10 digits',
                'contact_number_3.regex' => 'Contact number must be 10 digits',
                'contact_number_4.regex' => 'Contact number must be 10 digits',
                'bdate.required' => 'Please enter birth date',
                'any_date.required' => 'Please enter anniversary date',
                'email.required' => 'Please enter email',
            ];

//            dd($request);
            $validator = Validator::make($request_data, [
                'name' => 'required',
                'contact_number' => 'numeric|required|regex:/^[0-9]{10}$/',
                'contact_number_1' => 'numeric|regex:/^[0-9]{10}$/',
                'contact_number_2' => 'numeric|regex:/^[0-9]{10}$/',
                'contact_number_3' => 'numeric|regex:/^[0-9]{10}$/',
                'contact_number_4' => 'numeric|regex:/^[0-9]{10}$/',
                'bdate' => 'required',
                //'any_date' => 'required',
                'email' => 'email|max:255',
            ], $messages);

            if($validator->fails())
            {
//                return redirect()->back()->withErrors($validator, 'login');
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if(!isset($request->wish_id)){
                $wish = new Wish();
            }else{
                $wish = Wish::find($request->wish_id);
            }

            $wish->full_name	 = $request->name;
            $contact_number[] = $request->contact_number;
            if(isset($request->contact_number_1) && $request->contact_number_1 != "")
            {
              $contact_number[] = $request->contact_number_1;

            }
            if(isset($request->contact_number_2) && $request->contact_number_2 != "")
            {
              $contact_number[] = $request->contact_number_2;

            }
            if(isset($request->contact_number_3) && $request->contact_number_3 != "")
            {
              $contact_number[] = $request->contact_number_3;

            }
            if(isset($request->contact_number_4) && $request->contact_number_4 != "")
            {
              $contact_number[] = $request->contact_number_4;

            }
            $wish->contact_number = implode(' / ', $contact_number);
            $wish->email	 = $request->email;
            $wish->birth_date  = date('Y-m-d', strtotime($request->bdate));
            $wish->anniversary_date	 = date('Y-m-d', strtotime($request->any_date));
            $wish->save();

            if(isset($request->wish_id)){
                return redirect()->back()->with('success','Wish Update successfully');
            }
            return redirect()->back()->with('success','Wish added successfully');
        }catch (Exception $exception){
            return redirect()->back()->with('error',$exception);
        }
    }

    public function wishView(Request $request){
        $wish = Wish::find($request->id);
        return view('admin.view_wish')->with('wish',$wish)->with('menu', 'wishes')->with('sub_menu', 'view_users')->with('sms_balance', $this->sms_balance);
    }

    
    public function viewUsers(Request $request){  
    
      $request_data = $request->all();
      
      $current_date = date('d-m-Y');
      $start_date = date("d-m-Y",strtotime("-30days",strtotime($current_date)));
      $end_date = date('d-m-Y');
      
      if(isset($request_data['filter_date'])) {
        $range = $request_data['filter_date'];
        $start_date = explode('-',$range)[0];
        $end_date = explode('-',$range)[1];
      }
      
      $start_date = date("Y-m-d", strtotime(str_replace('/', '-', $start_date)));
      $end_date = date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));
        
      $users = DB::select("SELECT * FROM wishes WHERE date(created_at) >= '$start_date' AND date(created_at) <= '$end_date' ORDER BY created_at desc");
      
      return view('admin.view_users')
      ->with('users',$users)
      ->with('start_date', date("d/m/Y", strtotime($start_date)))
      ->with('end_date', date("d/m/Y", strtotime($end_date)))
      ->with('menu', 'wishes')->with('sms_balance', $this->sms_balance)
      ->with('sub_menu', 'view_users');
    }
    
    public function viewWishes(Request $request){  
    
      $request_data = $request->all();
      
      $start_date = date("Y-m-d");
      $end_date = date("Y-m-d",strtotime("+7days",strtotime($start_date)));
      $type = 0;
      
      if(isset($request_data['type'])) {
        $type = $request_data['type'];
      }
      
      if(isset($request_data['filter_date'])) {
        $range = $request_data['filter_date'];
        $start_date = explode('-',$range)[0];
        $end_date = explode('-',$range)[1];
        $start_date = date("Y-m-d", strtotime(str_replace('/', '-', $start_date)));
        $end_date = date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));
      }
      
      $date1=date_create($start_date);
      $date2=date_create($end_date);
      $diff=date_diff($date1,$date2);
      
      $array_wishes_list = array();

      for($i=0; $i<$diff->days; $i++)
      {
        $date = date("Y-m-d",strtotime("+". $i ."days",strtotime($start_date)));
        $startday = date("d", strtotime(str_replace('/', '-', $date)));
        $startmonth = date("m", strtotime(str_replace('/', '-', $date)));        

        if($type == 1) { // Birth date
          $wishes = Wish::whereDay('birth_date', '=', $startday)
          ->whereMonth('birth_date', '=', $startmonth)->get();
          for($w=0; $w<count($wishes); $w++)
          {
            $obj_wishes_list = new WishesList();
            $obj_wishes_list->id = $wishes[$w]->id;
            $obj_wishes_list->full_name = $wishes[$w]->full_name;
            $obj_wishes_list->view_date = $wishes[$w]->birth_date;
            //$obj_wishes_list->anniversary_date = $wishes[$w]->anniversary_date;
            $obj_wishes_list->contact_number = $wishes[$w]->contact_number;
            $obj_wishes_list->email = $wishes[$w]->email;
            $obj_wishes_list->view_type = 'Birthday';
            $array_wishes_list[] = $obj_wishes_list;
          }
          //$users = DB::select("SELECT * FROM wishes WHERE (day(birth_date) = '$startday' AND month(birth_date) >= '$startmonth')");
        }
        elseif($type == 2) { // Anniversary
          $wishes = Wish::whereDay('anniversary_date', '=', $startday)
          ->whereMonth('anniversary_date', '=', $startmonth)->get();
          for($w=0; $w<count($wishes); $w++)
          {
            $obj_wishes_list = new WishesList();
            $obj_wishes_list->id = $wishes[$w]->id;
            $obj_wishes_list->full_name = $wishes[$w]->full_name;
            //$obj_wishes_list->birth_date = $wishes[$w]->birth_date;
            $obj_wishes_list->view_date = $wishes[$w]->anniversary_date;
            $obj_wishes_list->contact_number = $wishes[$w]->contact_number;
            $obj_wishes_list->email = $wishes[$w]->email;
            $obj_wishes_list->view_type = 'Anniversary';
            $array_wishes_list[] = $obj_wishes_list;
          }
          //$users = DB::select("SELECT * FROM wishes WHERE (day(anniversary_date) >= '$startday' AND day(anniversary_date) <= '$endday' AND month(anniversary_date) >= '$startmonth' AND month(anniversary_date) <= '$endmonth')");
        }
        else {
          $birthdays = DB::select("SELECT * FROM wishes WHERE (day(birth_date) = '$startday' AND month(birth_date) = '$startmonth')");
          $anniversaries = DB::select("SELECT * FROM wishes WHERE (day(anniversary_date) = '$startday' AND month(anniversary_date) = '$startmonth')");   
          for($w=0; $w<count($birthdays); $w++)
          {
            $obj_wishes_list = new WishesList();
            $obj_wishes_list->id = $birthdays[$w]->id;
            $obj_wishes_list->full_name = $birthdays[$w]->full_name;
            $obj_wishes_list->view_date = $birthdays[$w]->birth_date;
            //$obj_wishes_list->anniversary_date = $birthdays[$w]->anniversary_date;
            $obj_wishes_list->contact_number = $birthdays[$w]->contact_number;
            $obj_wishes_list->email = $birthdays[$w]->email;
            $obj_wishes_list->view_type = 'Birthday';
            $array_wishes_list[] = $obj_wishes_list;
          }  
          for($w=0; $w<count($anniversaries); $w++)
          {
            $obj_wishes_list = new WishesList();
            $obj_wishes_list->id = $anniversaries[$w]->id;
            $obj_wishes_list->full_name = $anniversaries[$w]->full_name;
            //$obj_wishes_list->birth_date = $anniversaries[$w]->birth_date;
            $obj_wishes_list->view_date = $anniversaries[$w]->anniversary_date;
            $obj_wishes_list->contact_number = $anniversaries[$w]->contact_number;
            $obj_wishes_list->email = $anniversaries[$w]->email;
            $obj_wishes_list->view_type = 'Anniversary';
            $array_wishes_list[] = $obj_wishes_list;
          }           
        }
      }

      $start_date = date("Y-m-d", strtotime(str_replace('/', '-', $start_date)));
      $end_date = date("Y-m-d", strtotime(str_replace('/', '-', $end_date)));      

      
      return view('admin.view_wishes')
      ->with('users', $array_wishes_list)
      ->with('type',$type)
      ->with('start_date', date("d/m/Y", strtotime($start_date)))
      ->with('end_date', date("d/m/Y", strtotime($end_date)))
      ->with('menu', 'wishes')->with('sms_balance', $this->sms_balance)
      ->with('sub_menu', 'view_wishes');
    }    


    public function wishesHistory(Request $request){
        $type = '';

        $wishes = Wish::all();
        $anniversarys = Wish::all();
        $request_data = $request->all();
        $start_date = date("Y-m-d");
        $end_date = date("Y-m-d",strtotime("+7days",strtotime($start_date)));


        if(isset($request_data['filter_date'])) {
          $range = $request_data['filter_date'];
          $start_date = explode('-',$range)[0];
          $end_date = explode('-',$range)[1];
        }
        if(isset($request->filter_date)){
            $wishes = Wish::select('*');
            $anniversarys = Wish::select('*');
            $start_date = explode('-',$request->filter_date)[0];
            $end_date = explode('-',$request->filter_date)[1];

            $date_range = $request->filter_date;
            $wishes = $wishes->where('birth_date','>=',date('Y-m-d',strtotime($start_date))) ->where('birth_date','<=',date('Y-m-d',strtotime($end_date))) ;
            $anniversarys = $anniversarys->where('anniversary_date','>=',date('Y-m-d',strtotime($start_date))) ->where('anniversary_date','<=',date('Y-m-d',strtotime($end_date))) ;

        }

        if(isset($request->filter_date)){
            $type = $request->type;
            $wishes = $wishes->get();
//            dd($wishes);
            $anniversarys = $anniversarys->get();
        }
//        dd($wishes);


          foreach ($wishes as $wish){
              $wish->date = $wish->birth_date;
              $wish->type = 'Birth day';
          }



        foreach ($anniversarys as $anniversary){
            $anniversary->date = $anniversary->anniversary_date;
            $anniversary->type = 'Anniversary date';
        }

        return view('admin.wish_history')->with('wishes',$wishes)->with('type',$type)->with('anniversarys',$anniversarys)->with('start_date',$start_date)->with('end_date',$end_date)->with('menu', 'wishes')->with('sub_menu', 'wishes')->with('sms_balance', $this->sms_balance);
    }

    public function wishDelete(Request $request){

        $x = Wish::find($request->id)->delete();
        return redirect()->back()->with('success','Wish delete successfully');
    }
    
    public function importWishes(Request $request){
      return view('admin.import_wishes')->with('menu', 'wishes')->with('sub_menu', 'import_wishes')->with('sms_balance', $this->sms_balance);
    }
    
  //Post import excel
  public function postImportWishes(Request $request)
  { 
    $request_data = $request->all();
    
    $messages = [
      'excel-file.required' => 'Please select excel file name.'
    ];
    
    $validator = Validator::make($request_data, [
      'excel-file' => 'required'
    ], $messages);
    
    if($request['excel-file'] == null || $request['excel-file'] == "")
    {
      return redirect()->back()
      ->withInput()
      ->withErrors([
        'excel-error' 			=> 'Please select a excel file',
      ]);
    }
    
    if($validator->fails())
    { 
      return redirect()->back()->withErrors($validator)->withInput();
    }
    else
    {
      if(Input::hasFile('excel-file')){
        $path = Input::file('excel-file')->getRealPath();
        $extension = Input::file('excel-file')->getClientOriginalExtension(); 
        if(strtolower($extension) != 'xls' && strtolower($extension) != 'xlsx'){
          //if file is not excel
          return redirect()->back()
            ->withInput()
            ->withErrors([
            'excel-error' 			=> 'Please upload a valid file',
          ]);
        }
        $data = Excel::load($path, function($reader) {})->get();        
        $totalRows = $data->count();
        if($totalRows > 500)
        {
          return redirect()->back()
            ->withInput()
            ->withErrors([
            'excel-error' 			=> 'Please import maximum 500 users',
          ]);
        }
        // Start transaction!
        DB::beginTransaction();
        foreach($data as $row){
          if(isset($row['full_name']) && $row['full_name'] != null && $row['contact_number'] != null && strlen($row['contact_number']) == 10) {   
            $full_name = $row['full_name'];          
            $email = $row['email'];          
            $contact_number_0 = $row['contact_number']; 
            $contact_number_1 = $row['contact_number_1']; 
            $contact_number_2 = $row['contact_number_2']; 
            $contact_number_3 = $row['contact_number_3']; 
            $contact_number_4 = $row['contact_number_4']; 
            $contact_numbers = array();
            $contact_numbers[] = $contact_number_0;
            if($contact_number_1 != "")
            {
              $contact_numbers[] = $contact_number_1;
            }
            if($contact_number_2 != "")
            {
              $contact_numbers[] = $contact_number_2;
            }
            if($contact_number_3 != "")
            {
              $contact_numbers[] = $contact_number_3;
            }
            if($contact_number_4 != "")
            {
              $contact_numbers[] = $contact_number_4;
            }
            $contact_number = implode(' / ', $contact_numbers);
            
            $birthday_date = $row['birthday_date']; 
            $anniversary_date = $row['anniversary_date']; 
            if($email == "")
            {
              $email = '';
            } 
            if($birthday_date == "")
            {
              $birthday_date = '';
            }            
            if($anniversary_date == "")
            {
              $anniversary_date = '';
            } 
             
            try {
                DB::insert('INSERT INTO wishes (full_name, contact_number, email, birth_date, anniversary_date, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)', [$full_name, $contact_number, $email, $birthday_date, $anniversary_date, date('Y-m-d'), date('Y-m-d')]);
            } catch(ValidationException $e)
            {
                DB::rollback();
                return Redirect::to('/form')
                    ->withErrors( $e->getErrors() )
                    ->withInput();
            } catch(\Exception $e)
            {
                DB::rollback();
                throw $e;
            }         
          }
          else
          {
            DB::rollback();     
            return redirect()->back()
              ->withInput()
              ->withErrors([
              'excel-error' 			=> 'Please enter correct contact number',
            ]);
          }
        }
        try {
            DB::commit();
            return redirect()->back()->with('success', 'User list added successfully'); 
        } catch(ValidationException $e)
        {
            DB::rollback();
            return Redirect::to('/form')
                ->withErrors( $e->getErrors() )
                ->withInput();
        } catch(\Exception $e)
        {
            DB::rollback();
            throw $e;
        }
      }
      else{
        return redirect()->back()
        ->withInput()
        ->withErrors([
          'excel-error' 			=> 'Please select a excel file',
        ]);
      }
    }
  }  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

Class WishesList{}
