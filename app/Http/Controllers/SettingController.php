<?php

namespace App\Http\Controllers;

use App\Message;
use App\Notification;
use App\User;
use Illuminate\Http\Request;

use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    public function __construct(HomeController $home_controller)
    {
      $this->home_controller = $home_controller;
      $this->sms_balance = $this->home_controller->getSMSBalance();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Settings()
    {
            $message = '';
            $check = Message::all();
        if(isset($check[0])){
            $message = $check[0];
        }
        $notifactions = User::where('id', '!=', 1)->get();


       return view('admin.settings')->with('menu','settings')->with('message',$message)->with('notifactions',$notifactions)->with('sms_balance', $this->sms_balance);
    }

    public function SettingsSave(Request $request){
        $request_data = $request->all();

        $messages = [
            'birthday_message.required' => 'Please enter birthday message',
            'anniversary_message.required' => 'Please enter anniversary message',

        ];

        $validator = Validator::make($request_data, [
            'birthday_message' => 'required',
            'anniversary_message' => 'required',
        ], $messages);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
       $check = Message::all();
        if(isset($check[0])){
           $msg = Message::find($check[0]->id);
        }else{
            $msg = new Message();
        }
        $msg->birthday_message = $request->birthday_message;
        $msg->anniversary_message = $request->anniversary_message;
        $msg->save();
        return redirect()->back()->with('success','Message add successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notifactionDelete(Request $request){
        $xx= User::find($request->id)->delete();
        return redirect()->back()->with('success','User deleted successfully');
    }
    public function addNotification(Request $request)
    {
        if(isset($request->id)){
            $notification = User::find($request->id);
            return view('admin.add_notification')->with('menu','settings')->with('notification',$notification)->with('sms_balance', $this->sms_balance);
        }else{
            return view('admin.add_notification')->with('menu','settings')->with('sms_balance', $this->sms_balance);
        }

    }
    public function notificationSave(Request $request){
        $request_data = $request->all();
 
        $messages = [
            'name.required' => 'Please enter name',
            'email.required' => 'Please enter email',
            'number.required' => 'Please enter number',
            'password.required' => 'Please enter password',

        ];
        if(!isset($request->id)){
          $validator = Validator::make($request_data, [
              'name' => 'required',
              'email' => 'required|unique:users,email,$this->id,id',
              'number' => 'required',
              'password' => 'required|min:8',
          ], $messages);
        }
        else{
           $validator = Validator::make($request_data, [
              'name' => 'required',
              'email' => 'required|email',
              'number' => 'required'
          ], $messages);
        }

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if(!isset($request->id)){
            $notification = new User();
        }else{
          $notification = User::find($request->id);
        
          if(User::where('email',$request->email)->where('id', '!=', $request->id)->exists()){
              return redirect()->back()->withErrors(array('email' => 'Email already exists'));
          }
        }
        $notification->name = $request->name;
        $notification->email = $request->email;
        $notification->number = $request->number;
        $notification->password = Hash::make($request->password);
        $notification->save();

        return redirect()->back()->with('success','User added successfully');


    }
    
    public function updatePassword(Request $request){
      if(Auth::Check())
      {
        $request_data = $request->All();
        $messages = [
          'current-password.required' => 'Please enter current password',
          'password.required' => 'Please enter password',
        ];
        
        $validator = Validator::make($data, [
          'current-password' => 'required|min:8',
          'password' => 'required|min:8|same:password',
          'password_confirmation' => 'required|min:8|same:password',     
        ], $messages);
        if($validator->fails())
        {
          return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {  
          $current_password = Auth::User()->password;	        
          if(Hash::check($request_data['current-password'], $current_password))
          {		      
            $user_id = Auth::User()->id;	                   
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make($request_data['password']);;
            $obj_user->save(); 
            return redirect()->back()->with('success','Password updated successfully!');
          }
          else
          {           
            $validator->getMessageBag()->add('current-password', 'Please enter correct current password');  
            return redirect()->back()->withErrors($validator)->withInput();  
          }
        }        
      }
      else
      {
        return redirect()->to('/');
      } 


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
